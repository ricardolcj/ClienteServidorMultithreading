package t1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

class Servidor implements Runnable{
	
	private static int contador = 1;
	
	private Socket cliente;
	private Thread t;
	private int idThread;
	
	Servidor(Socket cliente){
		this.cliente = cliente;
		this.idThread = contador++;
	}
	
	private static boolean isPrime(int num) {
		if (num <= 1)
			return false;
		
		for (int i = 2; i <= Math.sqrt(num); i++) {
			if (num % i == 0)
				return false;
		}	
		
		return true;
    }

	public void run(){

		try {

			/*Abre entrada e saída de dados*/
		    BufferedReader entradaSocket = new BufferedReader(new InputStreamReader(cliente.getInputStream()));
		    PrintWriter saidaSocket = new PrintWriter(cliente.getOutputStream(), true);
		    
		    /*Recebe o número enviado pelo cliente*/
		    int numPedido = Integer.parseInt(entradaSocket.readLine());
		    
		    System.out.println("T" + idThread + ": Numero pedido: " + numPedido);
		    
		    /*Calcula se é primo e envia a resposta para o cliente*/
		    saidaSocket.println(Servidor.isPrime(numPedido));
		    
		    System.out.println("Saindo da Thread T"+ idThread);
		    
		    /*Fecha a conexão com o cliente*/
		    cliente.close();
			
		} catch (IOException e) {
			
		}
	}
	
	public void start() {
		System.out.println("Criando a Thread worker T"+ idThread);
		
		if( t == null) {
			t = new Thread(this);
			t.start();
		}
	}
}