package t1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.concurrent.ThreadLocalRandom;

class ThreadCliente implements Runnable {
	
	private static int contador = 1;
	  
	private Socket cliente;
	private Thread t;
	private int idThread;
  
	ThreadCliente(){
		this.idThread = contador++;
	}
  
	private Socket initCliente(){
	    try {
	    	cliente = new Socket("127.0.0.1", 3061);
	        
	    } catch (IOException ex) {
	        
	    }
	    return cliente;
	}

	public void run() {
		while(true) {
			try {
				/*Inicializa a conexão com o servidor*/
				this.cliente = initCliente();

				/*Gera um número aleatório entre 2 e 1000000*/
				int num = ThreadLocalRandom.current().nextInt(2, 1000000);

				/*Abre entrada e saída de dados*/
				PrintWriter out = new PrintWriter(cliente.getOutputStream(), true);
				BufferedReader in = new BufferedReader(new InputStreamReader(cliente.getInputStream()));

				/*Envia o número para o servidor*/
				out.println(num);

				/*Recebe a resposta e printa no terminal*/
				if(Boolean.parseBoolean(in.readLine()))
					System.out.printf("C%d: %d é primo\n", idThread, num);
				else
					System.out.printf("C%d: %d NÃO é primo\n", idThread, num);

				/*Coloca a Thread pra dormir*/
			    Thread.sleep(50);
			}	
			catch(Exception e) {
				
			}
		}
	}
	
	public void start() {
		System.out.println("Criando cliente C" + idThread);
		
		if( t == null) {
			t = new Thread(this);
			t.start();
		}
	}
}