package t1;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class dispatcher {
	public static void main(String[] args) throws IOException {
		
		@SuppressWarnings("resource")
		/*Abre o dispatcher na porta 3061*/
		ServerSocket server = new ServerSocket(3061);

		while(true) {
			/*Espera um novo cliente*/
			Socket cliente = server.accept();
			
			/*Atribui uma nova thread ao cliente conectado*/
			new Servidor(cliente).start();	    
		}	
	}
}
